package html.random_color.controller;

import org.springframework.web.bind.annotation.RequestMapping;

@org.springframework.stereotype.Controller
public class Controller {
    @RequestMapping(value = "/index")
    public String get() {
        return "index";
    }
}
